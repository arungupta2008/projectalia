package dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 22/12/16.
 */
@Getter
@Setter
public class PhoneNo {
    String no;

    // Validation should present here.
}

