package dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 22/12/16.
 */
@Getter
@Setter
@AllArgsConstructor
public class Name {
    String firstName;
    String secondName;

}
