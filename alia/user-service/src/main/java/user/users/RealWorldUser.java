package user.users;

import lombok.Getter;
import lombok.Setter;
import user.User;

/**
 * Created by arun.gupta on 22/12/16.
 */
@Getter
@Setter
public class RealWorldUser extends User {
    public RealWorldUser(String userId) {
        super(userId);
    }

    String userName;
    String passwordHash;
}
