package model;

import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
@Entity
@Table(name = "message_conversation_mapping")
public class MessageConversationMapping {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "converationId", nullable = false)
    private Long Id;


    // Intentionally Lazy Fetch
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "Id", nullable = false, updatable = false)
    private User sender;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "Id", nullable = false, updatable = false)
    private Message message;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "converation_id", nullable = false, updatable = false)
    private Conversation conversation;


    // Auto Populated in DB -- Here kept it for FYI
    @Column(nullable = false)
    Timestamp createDateTime;

}
