package model;

import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */

@Getter
@Setter
@Entity
@Table(name = "message")
public class Message {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    // I Kept it as One to many because same message can be forwarded by multiple people.
    // So we don't have to create Same message entries Always
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "Id", nullable = false, updatable = false)
    private DataStore dataStore;

    // Auto Populated in DB -- Here kept it for FYI
    @Column(nullable = false)
    Timestamp createDateTime;
}
