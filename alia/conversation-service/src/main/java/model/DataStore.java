package model;

import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
@Entity
@Table(name = "dataStore")
public class DataStore {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(nullable = false)
    String dataType;

    @Column(nullable = false)
    String dataUri;

    // Auto Populated in DB -- Here kept it for FYI
    @Column(nullable = false)
    Timestamp createDateTime;
}
