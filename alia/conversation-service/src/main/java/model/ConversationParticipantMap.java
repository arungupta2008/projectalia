package model;

import java.sql.Timestamp;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
@Entity
@Table(name = "conversation_participant_map")
/*
*  Keeps the Map between Conversation and Participants, Let's say in future you want to send
*  Broadcast or multicast type of messages. then we can use this capability.
*
*  For conversation between Alia and User. Here both Entry will be present.
* */
public class ConversationParticipantMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId", nullable = false, updatable = false)
    private User user;


    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "conversation_id", nullable = false, updatable = false)
    private Conversation conversation;

    // Auto Populated in DB -- Here kept it for FYI
    @Column(nullable = false)
    Timestamp createDateTime;


    // Stores the State of participation of conversation.
    @Column(nullable = false)
    boolean state;


    // Stoers the Seens Status of User or Alia
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id", nullable = false, updatable = false)
    private Message lastSeenMessageId;





}
