package model;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
@Entity
@Table(name = "conversation")
public class Conversation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "converation_id", nullable = false)
    private Long Id;

    // Auto Populated in DB -- Here kept it for FYI
    @Column(nullable = false)
    Timestamp createDateTime;

    // Auto Populated in DB -- Here kept it for FYI -- Not needed here too.  Means MySql 5.6 will solve the problem of auto population
    @Column(nullable = false)
    Timestamp updateDateTime;

    @Column(nullable = false)
    String name;


    // There is no entry of this Mapping
    @Transient
    @OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="conversation_id")
    List<ConversationParticipantMap> conversationParticipantMap;
}
