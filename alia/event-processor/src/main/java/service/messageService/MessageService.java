package service.messageService;

import dto.Event;
import dto.events.MessageEvent;
import dto.events.MessageReadDeliveredEvent;

/**
 * Created by arun.gupta on 23/12/16.
 */
public class MessageService {

    /*
    *  This Either Sends message to Bot Service or the User
     *  Before that it saves the conversation through conversation Service.
    * */
    public Event processMessageEvent(MessageEvent o) {
        // It's Event Based.
        persistMessage(o);
        /*
        * Here this on the basis of message and the Sender it will identify should it goto BotService or not.
        * */
        return null;
    }

    // Saves the Message to Conversation Service.
    private void persistMessage(MessageEvent o) {

    }
    // We will get the Session


    /*
    * This Stores the Message Read/ Delivered time and sends the event to User back if Bot have read the Message.
     * or just updates the counter.
    * */
    public Event processMessageEvent(MessageReadDeliveredEvent o) {
        // It's Event Based.
        /*
        * Here this on the basis of message and the Sender it will identify should it goto BotService or not.
        * */
        return null;
    }

}
