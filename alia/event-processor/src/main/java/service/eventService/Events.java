package service.eventService;

import java.io.Serializable;

/**
 * Created by arun.gupta on 23/12/16.
 */
public enum Events implements Serializable{
    // User Related Event
    register_user, user_got_registered,

    // Message Event
    message, message_delivered, message_read,

    // HTTP Persistence  request and response.
    keep_alive_request_event, keep_alive_response_event;
}
