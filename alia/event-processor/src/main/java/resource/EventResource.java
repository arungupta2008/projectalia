package resource;

import dto.Event;
import dto.events.MessageEvent;
import dto.events.MessageReadDeliveredEvent;
import dto.events.UserRegistrationEvent;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import service.messageService.MessageService;
import service.userService.UserService;
import service.eventService.Events;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Slf4j
// TODO: this should be /cdm/agent to make it cdm specific for checks. version 2
@Path("/event")
public class EventResource {

    @POST
    @Path("/processEvent")
    @Produces(MediaType.WILDCARD)
    @Consumes(MediaType.WILDCARD)
    public Response processEvent(){
        // Persistent Http
        return null;
    }


    @POST
    @Path("/processEvent")
    @Produces(MediaType.WILDCARD)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response processEventJson(Event event){

        /*
        *  I did not got time to implement Persistent HTTP (BOSH)
        * */
        // Persistent Http


        Event responseEvent = null;
        // Persistent Http
        if(event instanceof UserRegistrationEvent){
            UserRegistrationEvent O = (UserRegistrationEvent) event;
            if(O.getEvent() == Events.register_user){
                // Register User --
                // Either by using the API(If User service in Different Service) or by Service Call(If in Same Machine)
                UserService userService = new UserService();
                userService.registerUser(O);
            }if(O.getEvent() == Events.user_got_registered){
                // Send Welcome Message How ?
                MessageEvent messageEvent = createMessageForBotForNewUserOnboard();
                responseEvent =  sendEvent(messageEvent);
            }
        }else if(event instanceof MessageEvent){
            MessageService messageService = new MessageService();
            MessageEvent O = (MessageEvent) event;
            responseEvent = messageService.processMessageEvent(O);
        }else if(event instanceof MessageReadDeliveredEvent){
            MessageService messageService = new MessageService();
            MessageReadDeliveredEvent O = (MessageReadDeliveredEvent) event;
            responseEvent = messageService.processMessageEvent(O);
        }
        return Response.status(HttpStatus.SC_OK).entity(event.toString()).build();
    }

    private Event sendEvent(Event messageEvent) {
        // Send Event to Bot Service
        return null;
    }

    @POST
    @Path("/processEvent")
    @Produces(MediaType.WILDCARD)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response processEventConsumeMultiPart(){
        // Persistent Http
        return null;
    }


    private MessageEvent createMessageForBotForNewUserOnboard() {
        return null;
    }
}
