package dto;

import java.io.Serializable;
import lombok.Getter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
public abstract class Event implements Serializable{
   private String eventId;

    public Event(String eventId) {
        this.eventId = eventId;
    }
}
