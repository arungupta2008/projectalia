package dto.events;

import dto.Event;
import service.eventService.Events;
import java.util.Date;
import java.util.UUID;

/**
 * Created by arun.gupta on 23/12/16.
 */
public class MessageReadDeliveredEvent extends Event{

    public MessageReadDeliveredEvent() {
        super(UUID.randomUUID().toString());
    }

    Events event;
    private String messageId;
    private Date dateTime;
    private String messageReadByUserId;
    private String messageSentByUserId;
}
