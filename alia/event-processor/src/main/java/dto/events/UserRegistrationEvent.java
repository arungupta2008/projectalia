package dto.events;

import dto.Event;
import dto.Name;
import dto.PhoneNo;
import service.eventService.Events;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
public class UserRegistrationEvent extends Event{

    public UserRegistrationEvent() {
        super(UUID.randomUUID().toString());
    }
    Events event;
    private Name name;
    private PhoneNo phoneNo;
}
