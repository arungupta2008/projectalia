package dto.events;

import dto.Event;
import service.eventService.Events;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by arun.gupta on 23/12/16.
 */
@Getter
@Setter
public class MessageEvent extends Event{
    public MessageEvent() {
        super(UUID.randomUUID().toString());
    }
    Events event;
    String senderId;
    String receiverId;
    String messageId;
}
