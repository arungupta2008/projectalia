package dto.events;

import dto.Event;
import java.util.Date;
import java.util.UUID;

/**
 * Created by arun.gupta on 23/12/16.
 */
public class KeepAliveEvent extends Event {
    public KeepAliveEvent() {
        super(UUID.randomUUID().toString());
    }

    private int TTLinMs;
    private Date currentDateTime;
}
